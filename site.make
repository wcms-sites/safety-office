core = 7.x
api = 2

; uw_css_safety_office
projects[uw_css_safety_office][type] = "module"
projects[uw_css_safety_office][download][type] = "git"
projects[uw_css_safety_office][download][url] = "https://git.uwaterloo.ca/wcms/uw_css_safety_office.git"
projects[uw_css_safety_office][download][tag] = "7.x-1.0"
